// this script combines sound and moisture test
int plant_pin = 2;
int plant_read = 6;
int plant_value = 0;
int sound_out = 0;

bool moisture = false;

void setup() {
  pinMode(plant_pin, OUTPUT);
  //Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(plant_pin, HIGH);
  delay(500);
  plant_value = analogRead(plant_read);

  if (plant_value >= 150) {
    moisture = true;
    tone(sound_out,7000, 1000);
    delay(500);
    tone(sound_out,6000, 1000);
    delay(500);
    tone(sound_out,5000, 1000);
    delay(500);
    tone(sound_out,4000, 1000);
    delay(500);
    tone(sound_out,5000, 1000);
    delay(500);
    tone(sound_out,3000, 1000);
    delay(500);
    tone(sound_out,3500, 1000);
    delay(500);
  } else {
    moisture = false;
    tone(sound_out,100, 1000);
  }
  //Serial.println(plant_value);
  //Serial.println(moisture);
  digitalWrite(plant_pin, LOW);
  delay(1000);
}
