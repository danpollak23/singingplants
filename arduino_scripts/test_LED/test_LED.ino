#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(2, 4, NEO_GRB + NEO_KHZ800);

void setup() {
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1); //what is this?
#endif

TCCR0B = TCCR0B & B11111000 | B00000001; //no timer pre-scaler, fast PWM

  pixels.begin(); // This initializes the NeoPixel library.
  pixels.setBrightness(50);
  delay (100);
}

void loop() {
  pixels.setPixelColor(0, 254, 0, 254);
  pixels.setPixelColor(1, 0, 254, 0);
  pixels.show();
  
  delay(1000);
  
  pixels.setPixelColor(0, 0, 254, 0);
  pixels.setPixelColor(1, 254, 0, 254);
  pixels.show();
  delay(1000);

}
