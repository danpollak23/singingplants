// this script combines sound, moisture and light test
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
int light_out = 4;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(2, 4, NEO_GRB + NEO_KHZ800);

void setup() {
  #if defined (__AVR_ATtiny85__)
    if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
  #endif
  
  strip.setBrightness(30);
  strip.begin();
  strip.show();
}

void loop() {

  strip.setPixelColor(0, 255, 255, 255);
  strip.setPixelColor(1, 0, 255, 80);
  strip.show();
  delay(1000);
  strip.setPixelColor(0, 255, 0, 0);
  strip.setPixelColor(1, 0, 255, 0);
  strip.show();
  delay(1000);
}
