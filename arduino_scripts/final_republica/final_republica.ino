//https://github.com/sparkfun/H2OhNo/blob/master/firmware/H2OhNo/H2OhNo.ino

#include <avr/sleep.h> //Needed for sleep_mode
#include <avr/wdt.h> //Needed to enable/disable watch dog timer
int watchdog_counter;
int sound_out = 0;
int plant_pin = 1;
int plant_read = 2;
int plant_value = 0;
int i;
boolean first = true;

ISR(WDT_vect) {
  watchdog_counter++;
  //Don't do anything. This is just here so that we wake up.
}

void setup() {
  tone(sound_out,7000, 1000);
  delay(500);
  tone(sound_out,3000, 1000);
  delay(500);
  tone(sound_out,7000, 1000);
  delay(500);
  //Power down various bits of hardware to lower power usage  
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); //Power down everything, wake up from WDT
  sleep_enable();
}

void loop() {
  ADCSRA &= ~(1<<ADEN); //Disable ADC, saves ~230uA
  setup_watchdog(9); //Setup watchdog to go off after 1sec
  sleep_mode(); //Go to sleep! Wake up 1sec later and check water
  if(watchdog_counter > 5030 || first == true) // 5400 = about 12 h, 2700 = about 6 h (correction factor apparently) 1.075
  {
    watchdog_counter = 0;
    first = false; 

    ADCSRA |= (1<<ADEN); //Enable ADC

    digitalWrite(plant_pin, HIGH);
    delay(500);
    plant_value = analogRead(plant_read);
    digitalWrite(plant_pin, LOW);
    
    tone(sound_out,7000, 1000);
    delay(500);

    if (plant_value <= 100) {
      wdt_disable(); //Turn off the WDT!!
      while(plant_value <= 100){
        tone(sound_out,7000, 1000);
        delay(500);
        tone(sound_out,6000, 1000);
        delay(500);
        tone(sound_out,5000, 1000);
        delay(500);
        tone(sound_out,4000, 1000);
        delay(500);
        tone(sound_out,5000, 1000);
        delay(500);
        tone(sound_out,3000, 1000);
        delay(500);
        tone(sound_out,3500, 1000);
        delay(500);

        digitalWrite(plant_pin, HIGH);
        delay(500);
        plant_value = analogRead(plant_read);
        digitalWrite(plant_pin, LOW);
      }
    }

    
    delay(5000);
  }
}

//Sets the watchdog timer to wake us up, but not reset
//0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms
//6=1sec, 7=2sec, 8=4sec, 9=8sec
//From: http://interface.khm.de/index.php/lab/experiments/sleep_watchdog_battery/
void setup_watchdog(int timerPrescaler) {

  if (timerPrescaler > 9 ) timerPrescaler = 9; //Limit incoming amount to legal settings

  byte bb = timerPrescaler & 7; 
  if (timerPrescaler > 7) bb |= (1<<5); //Set the special 5th bit if necessary

  //This order of commands is important and cannot be combined
  MCUSR &= ~(1<<WDRF); //Clear the watch dog reset
  WDTCR |= (1<<WDCE) | (1<<WDE); //Set WD_change enable, set WD enable
  WDTCR = bb; //Set new watchdog timeout value
  WDTCR |= _BV(WDIE); //Set the interrupt enable, this will keep unit from resetting after each int
}
